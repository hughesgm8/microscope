Posts = new Mongo.Collection('posts');

// index Posts
if (Meteor.isServer) {
  Posts._ensureIndex( { title: 1, author: 1, tags: 1 } );
}

validatePost = function (post) {
  var errors = {};

  if (!post.title)
    errors.title = "Please fill in a headline";

  if (!post.url)
    errors.url =  "Please fill in a URL";

  return errors;
};

Meteor.methods({
  postInsert: function(postAttributes) {
    check(Meteor.userId(), String);
    check(postAttributes, {
      title: String,
      url: String,
      tags: Array
    });
      
    var errors = validatePost(postAttributes);
    if (errors.title || errors.url)
      throw new Meteor.Error('invalid-post', "You must set a title and URL for your post");
      
    var postWithSameLink = Posts.findOne({url: postAttributes.url});
    if (postWithSameLink) {
      return {
        postExists: true,
        _id: postWithSameLink._id
      }
    }

    var user = Meteor.user();
    // the _.extend() method is part of the Underscore library
    // it lets you “extend” one object with the properties of another
    var post = _.extend(postAttributes, {
      userId: user._id, 
      author: user.username, 
      submitted: new Date(),
      commentsCount: 0,
      upvoters: [],
      downvoters: [],
      votes: 0
    });

    var postId = Posts.insert(post);

    return {
      _id: postId
    };
  },
    
  updatePost: function(currentPostId, postProperties) {
    // check(Meteor.userId(), String); // not necessary, unless you want to check if the user is logged in and owns the Post, in that case check out ***** below
    check(currentPostId, String);
    check(postProperties, {
        title: String,
        url: String,
        tags: Array
    });
      
    var errors = validatePost(postProperties);
    if (errors.title || errors.url)
      throw new Meteor.Error('invalid-post', "You must set a title and URL for your post");

    var postWithSameLink = Posts.findOne({url: postProperties.url});
    if (postWithSameLink) {
      throw new Meteor.Error('duplicate', 'This URL is already being used by a Post');
    }
    
    // ***** Check post exists, user is logged in and user is the author.
    var post = Posts.findOne(currentPostId);
    var userId = Meteor.userId();
    if (!post || !userId || userId !== post.userId) {
      throw new Meteor.Error('unauthorized', 'User does not own this post!');
    }
      
    Posts.update(currentPostId, {$set: postProperties});
      
    return;
  },
    
  deletePost: function(currentPostId) {
      check(currentPostId, String);
      
      Posts.remove(currentPostId);
  },
    
  upvote: function(postId) {
    check(this.userId, String);
    check(postId, String);

    var affected = Posts.update({
      _id: postId,
      upvoters: {$ne: this.userId}
    }, {
      $addToSet: {upvoters: this.userId},
      $pull: {downvoters: this.userId},
      $inc: {votes: 1}
    });
    
    // insert notification into Notifications collection
    createVoteNotification(postId);

    if (! affected)
      throw new Meteor.Error('invalid', "You weren't able to upvote that post");
  },
    
  downvote: function(postId) {
    check(this.userId, String);
    check(postId, String);
      
    var affected = Posts.update({
      _id: postId,
      downvoters: {$ne: this.userId}
    }, {
      $addToSet: {downvoters: this.userId},
      $pull: {upvoters: this.userId},
      $inc: {votes: -1}
    });
    
    // insert notification into Notifications collection
    createVoteNotification(postId);
    
    if (! affected)
        throw new Meteor.Error('invalid', "You weren't able to downvote that post");
  }
});