Notifications = new Mongo.Collection('notifications');

Notifications.allow({
  update: function(userId, doc, fieldNames) {
    return ownsDocument(userId, doc) && 
      fieldNames.length === 1 && fieldNames[0] === 'read';
  }
});

duplicateNotification = function(dupId, dupType) {
    Notifications.remove({
      postId: dupId,
      type: dupType,
      read: false
    });
};

createCommentNotification = function(comment) {
  var post = Posts.findOne(comment.postId);

  duplicateNotification(comment.postId, 'comment');

  if (comment.userId !== post.userId) {
    Notifications.insert({
      userId: post.userId,
      postId: post._id,
      commentId: comment._id,
      commenterName: comment.author,
      type: 'comment',
      message: comment.author + ' commented on your post',
      read: false
    });
  }
};

createVoteNotification = function(postId) {
  var post = Posts.findOne(postId);
    
  duplicateNotification(postId, 'vote');
    
  Notifications.insert({
    userId: post.userId,
    postId: post._id,
    type: 'vote',
    message: 'Your post now has ' + post.votes + ' votes',
    read: false
  });
};