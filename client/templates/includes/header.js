Template.header.helpers({
  activeRouteClass: function(/* route names */) {
    var args = Array.prototype.slice.call(arguments, 0);
    args.pop();

    var active = _.any(args, function(name) {
      return Router.current() && Router.current().route.getName() === name
    });
    
    // JavaScript pattern boolean + string
    // returns false if boolean is false
    // returns string if boolean is true
    return active && 'active';
  }
});

/*
Template.header.events({
  'keyup [name="search"]': function(e) {
      e.preventDefault();
      
      let value = e.target.value.trim();
      
      if (value !== '' && e.keyCode === 13) {
          Router.go('search');
      }
  } 
});
*/
