Template.postItem.helpers({
  ownPost: function() {
    return this.userId == Meteor.userId();
  },
  domain: function() {
    var a = document.createElement('a');
    a.href = this.url;
    return a.hostname;
  },
  upvotedClass: function() {
    var userId = Meteor.userId();
    if (userId && !_.include(this.upvoters, userId)) {
      return 'btn-primary upvotable';
    } else {
      return 'disabled';
    }
  },
  downvotedClass: function() {
    var userId = Meteor.userId();
    if (userId && !_.include(this.downvoters, userId)) {
      return 'btn-primary downvotable';
    } else {
      return 'disabled'; 
    }
  },
  tag: function() {
    return this.tags;
  },
  // currently if no tags are entered on submit, "" is stored in "tags"
  isEmpty: function(attribute) {
    return attribute[0] === "";
  }
});

Template.postItem.events({
  'click .upvotable': function(e) {
    e.preventDefault();
    Meteor.call('upvote', this._id);
  },
  'click .downvotable': function(e) {
    e.preventDefault();
    Meteor.call('downvote', this._id);
  }
});