Template.postEdit.onCreated(function() {
  Session.set('postEditErrors', {});
});

Template.postEdit.helpers({
  errorMessage: function(field) {
    return Session.get('postEditErrors')[field];
  },
  errorClass: function (field) {
    return !!Session.get('postEditErrors')[field] ? 'has-error' : '';
  }
});

Template.postEdit.events({
  'submit form': function(e) {
    e.preventDefault();

    var currentPostId = this._id;

    var postProperties = {
      url: $(e.target).find('[name=url]').val(),
      title: $(e.target).find('[name=title]').val(),
      // eventually need to change this so you're not relying on the user
      // to include exactly one comma and exactly one space
      // ideally tags should be separated by the user pressing ENTER
      tags: _.uniq($(e.target).find('[name=tags]').val().split(/,\s*/))
    };
    
    Meteor.call('updatePost', currentPostId, postProperties, function(error, result) {
      if (error) 
        return throwError(error.reason);
        
      Router.go('postPage', {_id: currentPostId});
    });
  },
    
  'click .delete': function(e) {
    e.preventDefault();

    if (confirm("Delete this post?")) {
      var currentPostId = this._id;
      
      // Posts.remove(currentPostId);
      Meteor.call('deletePost', currentPostId, function(error, result) {
          if (error)
              return throwError(error.reason);
          
          Router.go('home');
      });
    }
  }
});