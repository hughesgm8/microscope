Template.notifications.helpers({
  notifications: function() {
    return Notifications.find({userId: Meteor.userId(), read: false});
  },
  notificationCount: function(){
    return Notifications.find({userId: Meteor.userId(), read: false}).count();
  }
});

Template.notificationItem.helpers({
  notificationPostPath: function() {
    return Router.routes.postPage.path({_id: this.postId});
  },
  typeIsComment: function() {
    return Notifications.findOne({postId: this.postId}, {fields: {type: 1}}).type == "comment";
  },
  voteCount: function() {
    return Posts.findOne({_id: this.postId}, {fields: {votes: 1}}).votes;
  }
});

Template.notificationItem.events({
  'click a': function() {
    Notifications.update(this._id, {$set: {read: true}});
  }
});