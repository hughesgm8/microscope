Template.searchBox.onCreated( () => {
  let template = Template.instance();

  template.searchQuery = new ReactiveVar();
  template.searching   = new ReactiveVar( false );

  template.autorun( () => {
    template.subscribe( 'searchResults', template.searchQuery.get(), () => {
      setTimeout( () => {
        template.searching.set( false );
      }, 300 );
    });
  });
});

Template.search.helpers({
  searching() {
    return Template.instance().searching.get();
  },
  query() {
    return Template.instance().searchQuery.get();
  },
  posts() {
    let posts = Posts.find();
    if ( posts ) {
      return posts;
    }
  }
});

Template.searchBox.events({
  'focus [name="searchBox"]' ( event ) {
     event.preventDefault();
      
     Router.go('search'); 
  },
  'blur [name="searchBox"]' ( event ) {
     event.preventDefault();
      
     Router.go('home'); 
  },
  'keyup [name="searchBox"]' ( event, template ) {
    //debugger;
    event.preventDefault();
      
    let value = event.target.value.trim();

    if ( value !== '' && event.keyCode === 13 ) {
      template.searchQuery.set( value );
      template.searching.set( true );
    }

    if ( value === '' ) {
      template.searchQuery.set( value );
    }
  }
});